import 'package:http/http.dart' as http;
import 'package:totov/repositories/authentication_repository.dart';

class AuthenticatedClient extends http.BaseClient {
  late final AuthenticationRepository _authenticationRepository;
  final http.Client? _inner;

  AuthenticatedClient(this._authenticationRepository, [this._inner]);

  Future<http.StreamedResponse> send(http.BaseRequest request) async {
    if (_authenticationRepository.authData == null) {
      throw new Exception(
          'Attempted to use authenticated client while not authenticated');
    }

    request.headers[AuthData.AccessTokenKey] =
        _authenticationRepository.authData!.accessToken;
    request.headers[AuthData.ClientKey] =
        _authenticationRepository.authData!.client;
    request.headers[AuthData.UidKey] = _authenticationRepository.authData!.uid;
    final http.Client client = (_inner == null ? http.Client() : _inner)!;

    final response = await client.send(request);
    if (response.headers[AuthData.AccessTokenKey] != null &&
        response.headers[AuthData.AccessTokenKey]!.isNotEmpty) {
      final newAuthData = AuthData(
          response.headers[AuthData.AccessTokenKey]!,
          response.headers[AuthData.ClientKey]!,
          response.headers[AuthData.UidKey]!);
      _authenticationRepository.saveAuthData(newAuthData);
    }
    return response;
  }
}
