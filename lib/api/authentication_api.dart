import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:totov/repositories/authentication_repository.dart';

class AuthenticationApi {
  static Future<bool> authenticate(String username, String password,
      AuthenticationRepository _authenticationRepository) async {
    try {
      final response = await http.post(
          Uri.http('localhost:3000', '/auth/sign_in'),
          headers: {HttpHeaders.contentTypeHeader: 'application/json'},
          body: jsonEncode({'email': username, 'password': password}));

      if (response.statusCode != 200 &&
          response.headers[AuthData.AccessTokenKey]!.isNotEmpty) {
        return false;
      }

      _authenticationRepository.saveAuthData(AuthData(
          response.headers[AuthData.AccessTokenKey]!,
          response.headers[AuthData.ClientKey]!,
          response.headers[AuthData.UidKey]!));
      return true;
    } catch (Exception) {
      return false;
    }
  }
}
