import 'dart:async';

enum AuthenticationStatus { unknown, authenticated, unauthenticated }

class AuthData {
  static const AccessTokenKey = 'access-token';
  static const UidKey = 'uid';
  static const ClientKey = 'client';

  AuthData(this.accessToken, this.client, this.uid);

  late final String accessToken;
  late final String client;
  late final String uid;
}

class AuthenticationRepository {
  final _controller = StreamController<AuthenticationStatus>();
  AuthData? authData;
  String? username;
  String? password;

  Stream<AuthenticationStatus> get status async* {
    await Future<void>.delayed(const Duration(seconds: 1));
    yield AuthenticationStatus.unauthenticated;
    yield* _controller.stream;
  }

  void saveAuthData(AuthData _authData) {
    authData = _authData;
  }

  void logIn() {
    _controller.add(AuthenticationStatus.authenticated);
  }

  void logOut() {
    _controller.add(AuthenticationStatus.unauthenticated);
  }

  void dispose() => _controller.close();
}
