import 'package:flutter/cupertino.dart';
import 'package:totov/app.dart';
import 'package:totov/repositories/authentication_repository.dart';
import 'package:totov/repositories/user_repository.dart';

void main() {
  runApp(App(
    authenticationRepository: AuthenticationRepository(),
    userRepository: UserRepository(),
  ));
}
