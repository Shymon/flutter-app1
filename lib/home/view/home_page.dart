import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:totov/api/authenticated_client.dart';
import 'package:totov/authentication/bloc/authentication_bloc.dart';
import 'package:totov/repositories/authentication_repository.dart';

class HomePage extends StatelessWidget {
  static Route route() {
    return MaterialPageRoute<void>(builder: (_) => HomePage());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Home')),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Builder(
              builder: (context) {
                final userId = context.select(
                  (AuthenticationBloc bloc) => bloc.state.user.id,
                );
                return Text('UserID: $userId');
              },
            ),
            ElevatedButton(
              child: const Text('Logout'),
              onPressed: () {
                context
                    .read<AuthenticationBloc>()
                    .add(AuthenticationLogoutRequested());
              },
            ),
            ElevatedButton(
              child: const Text('Fetch'),
              onPressed: () async {
                final _authenticationRepository =
                    RepositoryProvider.of<AuthenticationRepository>(context);
                AuthenticatedClient(_authenticationRepository)
                    .get(Uri.http('localhost:3000', '/'));
              },
            ),
          ],
        ),
      ),
    );
  }
}
